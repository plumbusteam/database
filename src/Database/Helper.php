<?php namespace Plumbus\Database;

use Doctrine\DBAL\Migrations\Tools\Console\Command as MigrationsCommand;

class Helper
{
    /**
     * @return SymfonyConsole[]
     */
    static public function getMigrationsCommands() : array
    {
        $commands = [];
        $commands[] = new MigrationsCommand\ExecuteCommand();
        $commands[] = new MigrationsCommand\GenerateCommand();
        $commands[] = new MigrationsCommand\LatestCommand();
        $commands[] = new MigrationsCommand\MigrateCommand();
        $commands[] = new MigrationsCommand\StatusCommand();
        $commands[] = new MigrationsCommand\VersionCommand();

        return $commands;
    }
}
