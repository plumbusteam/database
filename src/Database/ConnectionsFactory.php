<?php namespace Plumbus\Database;

use Doctrine\DBAL\DriverManager;
use Plumbus\Database\Exception\ConfigurationException;

/**
 * Class ConnectionsFactory
 * @package Plumbus\Database
 */
abstract class ConnectionsFactory
{
    /**
     * @var Connection[]
     */
    private $connections = [];

    /**
     * Возвращает настройки коннекта к базе данных по имени
     *
     * @param string $connectionName
     * @return array
     */
    abstract public function getConnectionSettings(string $connectionName):array;

    public function __get(string $connectionName):Connection
    {
        if (!isset($this->connections[$connectionName])) {
            $this->connections[$connectionName] = $this->createConnection($connectionName);
        }

        return $this->connections[$connectionName];
    }

    /**
     * @param string $connectionName
     * @return Connection
     * @throws ConfigurationException
     * @throws \Doctrine\DBAL\DBALException
     */
    private function createConnection(string $connectionName):Connection
    {
        $connectionConfig = $this->getConnectionSettings($connectionName);

        if (empty($connectionConfig['dbname'])) {
            throw new ConfigurationException('dbname parameter missed in configuration');
        }

        if (empty($connectionConfig['user'])) {
            throw new ConfigurationException('dbname parameter missed in configuration');
        }

        $connectionParams = [
            'dbname' => (string) $connectionConfig['dbname'],
            'user' => (string) $connectionConfig['user'],
            'password' => (string) $connectionConfig['password'] ?? '',
            'host' => (string) $connectionConfig['host'] ?? 'localhost',
            'driver' => (string) $connectionConfig['driver'] ?? 'mysqli',
            'charset' => (string) $connectionConfig['charset'] ?? 'utf8',
        ];
        $connection = new Connection(DriverManager::getConnection($connectionParams));
        if (!empty($connectionParams['charset'])) {
            $connection->query('SET NAMES ' . (string) $connectionParams['charset']);
        }

        return $connection;
    }
}