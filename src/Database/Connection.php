<?php namespace Plumbus\Database;

use Doctrine\DBAL\Types\Type;

class Connection
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    public $connection;

    /**
     * Connection constructor.
     * @param \Doctrine\DBAL\Connection $connection
     */
    public function __construct(\Doctrine\DBAL\Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param $query
     * @param array $parameters
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function query($query, array $parameters = [])
    {
        if ($parameters) {
            $types = $this->getTypes($parameters);
        } else {
            $types = [];
        }
        $result = $this->connection->executeQuery($query, $parameters, $types);
        return $result;
    }

    /**
     * @param string $query
     * @param array $parameters
     * @return mixed
     */
    public function selectValue(string $query, array $parameters = [])
    {
        $types = $this->getTypes($parameters);
        $result = $this->_query($query, $parameters, $types)->fetch(\PDO::FETCH_NUM)[0];
        return $result;
    }

    /**
     * @param string $query
     * @param array $parameters
     * @return array|null
     */
    public function selectRow(string $query, array $parameters = [])
    {
        $types = $this->getTypes($parameters);
        return $this->_query($query, $parameters, $types)->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $tableName
     * @param array $data
     * @param array|null $onDuplicateKeyUpdate
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function insert(string $tableName, array $data, array $onDuplicateKeyUpdate = null)
    {
        $params = array_values($data);
        $query = 'INSERT INTO ' . $tableName . ' (' . implode(', ', array_keys($data)) . ')' .
            ' VALUES (' . implode(', ', array_fill(0, count($data), '?')) . ')';

        if ($onDuplicateKeyUpdate) {
            $query .= ' ON DUPLICATE KEY UPDATE ';
            $queryParts = [];
            foreach ($onDuplicateKeyUpdate as $field) {
                $queryParts [] = '`' . $field . '`= ?';
                $params[] = $data[$field];
            }
            $query .= implode(',', $queryParts);
        }

        $result = $this->connection->executeUpdate(
            $query,
            $params,
            $this->getTypes($params)
        );

        return $result;
    }

    /**
     * @param string $query
     * @param array $params
     * @param string|null $keyNick
     * @return array
     */
    public function selectKeyRow(string $query, array $params = [], string $keyNick = null)
    {
        $result = [];

        foreach ($this->selectAll($query, $params) as $row) {
            $key = (null === $keyNick || !isset($row[$keyNick]) ? reset($row) : $row[$keyNick]);
            $result[$key] = $row;
        }

        return $result;
    }

    /**
     * @param string $query
     * @param array $parameters
     * @return array
     */
    public function selectColumn(string $query, array $parameters = [])
    {
        $result = [];
        foreach ($this->selectAll($query, $parameters) as $row) {
            $key = reset($row);
            $result[] = $key;
        }
        return $result;
    }


    /**
     * @param $query
     * @param array $params
     * @return array
     */
    public function selectAll($query, array $params = array())
    {
        $types = $this->getTypes($params);
        $result = $this->connection->fetchAll($query, $params, $types);
        return $result;
    }

    /**
     * @param string $tableName
     * @param array $ids
     * @param string $idField
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function deleteByIds(string $tableName, array $ids, string $idField = 'id')
    {
        $query = 'DELETE FROM `' . $tableName . '` WHERE `' . $idField . '` IN(' . implode(', ',
                array_fill(0, count($ids), '?')) . ')';

        return $this->connection->executeUpdate($query,
            $ids, $this->getTypes($ids));
    }

    /**
     * @return string
     */
    public function lastInsertId()
    {
        return $this->connection->lastInsertId();
    }

    /**
     * @param array $parameters
     * @return array
     */
    private function getTypes(array $parameters = [])
    {
        $types = [];
        foreach ($parameters as $parameter) {
            if (is_integer($parameter)) {
                $types[] = Type::STRING;
            } elseif (is_array($parameter)) {
                $types[] = \Doctrine\DBAL\Connection::PARAM_STR_ARRAY;
            } else {
                $types[] = Type::STRING;
            }
        }
        return $types;
    }

    /**
     * @param string $query
     * @param array $parameters
     * @param array $types
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    private function _query(string $query, array $parameters = [], array $types = [])
    {
        return $this->connection->executeQuery($query, $parameters, $types);
    }
}
